<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public static function getPosts()
    {
        $thisobj = new self();
        return $thisobj->orderBy('created_at', 'desc')->limit(4)->get();
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
