<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PagesController extends Controller
{
    static function getIndex()
    {
        $post = Post::getPosts();
    	return view('pages/welcome')->withPosts($post);
    }

    public function getAbout()
    {
    	$firstName = "Victor";
    	$secondName = "Gordeyev";
    	$fullName = $firstName . " " . $secondName;
    	$email = "gnexus460@gmail.com";
    	$data = [];
    	$data['email'] = $email;
    	$data['fullName'] = $fullName;
    	// return view('pages/about')->withFlname($fullName)->withEmail($email);
    	return view('pages/about')->withData($data);
    }

    public function getContacts()
    {
    	return view('pages/contacts');
    }
}
