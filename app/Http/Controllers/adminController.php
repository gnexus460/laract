<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class adminController extends Controller
{
    public function getIndex() {
       return view('admin.frontpage');
    }

    public function getPosts() {
        $posts = Post::all();
        $allPosts = [];
        $i = 0;
        foreach ($posts as $post) {
            $allPosts[$i]['id'] = $post->id;
            $allPosts[$i]['title'] = $post->title;
            $allPosts[$i]['image'] = $post->image;
            $allPosts[$i]['body'] = $post->body;
            $allPosts[$i]['slug'] = $post->slug;
            $allPosts[$i]['created_at'] = date($post->created_at);
            $allPosts[$i]['updated_at'] = date($post->updated_at);
            $i++;
        }
        return response()->json($allPosts);
    }
}
