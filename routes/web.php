<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => ['web']], function(){
    Route::get('blog/{slug}',['as' => 'blog.single', 'uses' => 'BlogController@getSingle'])->where('slug', '[\w\d\-\_]+');
    Route::get('blog',['as' => 'blog.index', 'uses' => 'BlogController@getIndex']);
    Route::get('/', 'PagesController@getIndex');
    Route::get('about', 'PagesController@getAbout');
    Route::get('contacts', 'PagesController@getContacts');
//    Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
//    Route::post('password/email');
});
Route::group(['middleware' => ['auth']], function(){
    Route::resource('posts', 'PostController');
    Route::resource('admin/pages', 'MakePagesController');
    Route::post('posts/upload', 'PostController@imageUploadPost')->name('image.upload.post');
    Route::get('admin', [
        'as' => 'admin.index',
        'uses' => 'adminController@getIndex'
    ]);
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('index');
