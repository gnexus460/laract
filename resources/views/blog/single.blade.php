@extends('main')
@section('title', "| $post->title")

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h1>{{ $post->title }}</h1>
                <hr/>
                <p>Posted In: {{ $post->category->name }}</p>
                <img src="{{asset("images/$post->image")}}" alt="">
                <p>{{ $post->body }}</p>
                <hr />
            </div>
        </div>
    </div>
@endsection