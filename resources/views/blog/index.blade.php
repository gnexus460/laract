@extends('main')
@section('title','| blog')

@section('content')

    <div class="container">
        <div class="col-md-8 offset-md-2">
            <h1>Blog</h1>
        </div>
        <div class="col-md-8 offset-md-2">
            @foreach($posts as $post)
                <div class="post">
                    <h2>{{ $post->title }}</h2>
                    <h5>Published: {{ date('M j Y', strtotime($post->created_at)) }}</h5>
                    <img src="{{asset("images/$post->image")}}" alt="">
                    <p>{{ substr($post->body, 0, 500) }} {{ strlen($post->body) > 50 ? '...' : '' }}</p>
                    <a href="{{ url('blog/'.$post->slug) }}" class="btn btn-primary">read more</a>
                    <hr />
                </div>
            @endforeach
        </div>
        <div class="col-md-8 offset-md-2 center-pagination">
            {{ $posts->links() }}
        </div>
    </div>

@endsection
