<form action="{{ route('image.upload.post') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-6">
            <input type="file" name="image" id="file_input">
        </div>
        <div class="col-md-6">
            <button type="submit" class="btn btn-success" id="loadImg">Загрузить</button>
        </div>
    </div>
</form>