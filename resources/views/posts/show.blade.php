@extends('main')

@section('title',"| $post->title")

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1 style="margin-bottom: 0;">{{ $post->title }}</h1>
                <hr />
                <p style="margin-top: 20px;" class="lead">{{ $post->body }}</p>
                <img src="{{asset("images/$post->image")}}" class="rounded float-left" alt="...">

            </div>
            <div class="col-md-4">
                <div class="card card-body bg-light">
                    <dl class="row">
                        <dt class="col-sm-5">URL Slug:</dt>
                        <dd class="col-sm-7"><a href="{{ url('blog/'.$post->slug ) }}">{{ url('blog/'.$post->slug) }}</a></dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-5">Created at:</dt>
                        <dd class="col-sm-7">{{ date('d \of\ F Y', strtotime($post->updated_at)) }}</dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-5">Last uploaded:</dt>
                        <dd class="col-sm-7">{{ date('d \of\ F Y', strtotime($post->updated_at)) }}</dd>
                    </dl>
                    <hr />
                    <div class="row">
                        <div class="col-sm-6">
                            {{ Html::linkRoute('posts.edit', 'Edit', array($post->id), array('class' => 'btn btn-primary btn-block')) }}
                        </div>
                        <div class="col-sm-6">
                            {{ Form::model($post, ['route' => ['posts.destroy', $post->id], 'method' => 'DELETE']) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection