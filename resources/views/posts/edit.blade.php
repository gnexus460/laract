@extends('main')

@section('title','| Edit Post')

@section('content')
    <div class="container">
        {{ Form::model($post, ['route' => ['posts.update', $post->id], 'method' => 'PUT']) }}
        <div class="row">
            <div class="col-md-8">
                {{ Form::label('title','Title:') }}

                {{ Form::text('title', null, ["class" => "form-control-lg form-control"]) }}

                {{ Form::label('slug','Slug:') }}
                {{ Form::text('slug', null, ['class' => 'form-control']) }}
                {{ Form::label('image','Image:') }}
                {{ Form::file('image', ['class' => "form-control", "value" => "$post->image"]) }}
                <hr />
                {{ Form::label('body','Body:') }}

                {{ Form::textarea('body', null, ["class" => "form-control"]) }}
            </div>
            <div class="col-md-4">
                <div class="card card-body bg-light">
                    <dl class="row">
                        <dt class="col-sm-5">Created at:</dt>
                        <dd class="col-sm-7">{{ date('d \of\ F Y', strtotime($post->updated_at)) }}</dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-5">Last uploaded:</dt>
                        <dd class="col-sm-7">{{ date('d \of\ F Y', strtotime($post->updated_at)) }}</dd>
                    </dl>
                    <hr />
                    <div class="row">
                        <div class="col-sm-6">
                            {{ Form::submit('Save', ['class' => 'btn btn-success btn-block']) }}
                        </div>
                        <div class="col-sm-6">
                            {{ Html::linkRoute('posts.show', 'Cancel', array($post->id), array('class' => 'btn btn-block')) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection