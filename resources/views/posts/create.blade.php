@extends('main')
@section('title','Create post')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h1>Create new post</h1>
                <hr />
                {!! Form::open(['route' => 'posts.store', 'enctype' => 'multipart/form-data']) !!}
                    {{ Form::label('title', 'Title:') }}
                    {{ Form::text('title', null, array('class' => 'form-control'))  }}
                    <br />
                    {{ Form::label('image', 'Image:') }}
                    <br />
                    {{ Form::file('image', null, array('class' => 'form-control'))  }}
                    <br />
                    {{ Form::label('slug', 'Slug:') }}
                    {{ Form::text('slug', null, array('class' => 'form-control'))  }}
                    {{ Form::label('body', 'Post Body:') }}
                    {{ Form::textarea('body', null, array('class' => 'form-control')) }}
                    {{ Form::submit('Create Post', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px')) }}
                {!! Form::close() !!}
{{--                @include('partials._upload-image')--}}
            </div>
        </div>
    </div>
@endsection