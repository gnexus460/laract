<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('partials._head')
<body>
    <div class="content">
    @include('partials._nav')
    @include('partials._messages')
        @if(Auth::check())

        @endif
    @include('partials._content')
    </div>
    @include('partials._footer')
</body>
</html>
