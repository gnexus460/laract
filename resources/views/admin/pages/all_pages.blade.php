@extends('admin.index')
@section('content')
<div class="container">
    <div class="create-page-block">
        <h2>Странницы</h2>
        {!! Form::open(['route' => 'pages.create']) !!}
        {{ Form::submit('Create Page', array('class' => 'btn btn-primary', 'style' => 'margin-top: 20px')) }}
        {!! Form::close() !!}
    </div>
</div>
@endsection