@extends('admin.index')
@section('content')
    <div class="container">
       <h2>Добавить страницу</h2>
        {!! Form::open(['route' => 'pages.store']) !!}
        {{ Form::label('', 'Title:') }}
        {{ Form::text('title', null, array('class' => 'form-control'))  }}
        {{ Form::label('slug', 'Slug:') }}
        {{ Form::text('slug', null, array('class' => 'form-control'))  }}
        {{ Form::label('description', 'Description:') }}
        {{ Form::textarea('description', null, array('class' => 'form-control'))  }}
        {{ Form::label('parent', 'Parent:') }}
        {{ Form::text('parent', null, array('class' => 'form-control'))  }}
        {{ Form::label('status', 'Status:') }}
        {{ Form::text('status', null, array('class' => 'form-control'))  }}
        {!! Form::close() !!}
    </div>
@endsection