<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('partials._head')
<body>
<div class="content">
    @include('partials._messages')
    @if(Auth::check())
    @endif
    @include('admin.partials.sidebar-nav')
    <div class="col-md-10 offset-md-2 content-container">
        @yield('content')
    </div>
</div>
    @include('.partials._footer')
</body>
</html>