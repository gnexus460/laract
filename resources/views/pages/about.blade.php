@extends('main')
@section('title', '| About')

@section('content')
  <div class="container">
      <div class="row">
          <div class="col-md-12">
              <h1>About me</h1>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam obcaecati dicta ipsum placeat voluptate tempora temporibus, eum quas corporis velit natus odit unde consequatur numquam, sunt culpa quaerat ipsa, facilis!</p>
          </div>
      </div>
  </div>
@endsection