@extends('main')

@section('title', '| Homepage')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="jumbotron">
          <h1 class="display-4">Welcome to my blog!</h1>
          <p class="lead">Thank you for visiting. This is my test web-site with laravel. Please read my last post</p>
          <hr class="my-4">
          <p class="lead">
            <a class="btn btn-primary btn-lg" href="#" role="button">Latest post</a>
          </p>
        </div>
      </div>
    </div>

    <div class="row">

      <div class="col-md-8">
        @foreach($posts as $post)
          <div class="post">
            <h3>{{ $post->title }}</h3>
            <img src="{{asset("images/$post->image")}}" alt="">
            <p>{{ substr($post->body, 0, 500) }} {{ strlen($post->body) > 50 ? '...' : '' }}</p>
            <a href="{{ url('blog/'.$post->slug) }}" class="btn btn-primary">read more</a>
          </div>
          <hr />
        @endforeach
      </div>

      <div class="col-md-3 offset-md-1">
        <h2>Sidebar</h2>
      </div>
    </div>
  </div>

@endsection