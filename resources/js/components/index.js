import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Navigation from './app-router'

$(document).ready(function () {
    if (document.getElementById('root')) {
        ReactDOM.render(<Navigation />, document.getElementById('root'));
    }
});