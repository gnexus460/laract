import React from 'react';

import { Router, Route, Link } from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import HomePage from './src/HomePage'
import Pages from './src/Pages'
import Posts from './src/Posts'

const history = createBrowserHistory();

const Home = () => (
    <div>
        <HomePage/>
    </div>
);
const PostDetail = () => (
    <div>
        <PostView/>
    </div>
);
const Page = () => (
    <div>
        <Pages/>
    </div>
);

const Post = () => (
    <div>
        <Posts />
    </div>
);

const News = () => (
    <div>
        <NewsPage />
    </div>
);

class Navigation extends React.Component{
    render() {
        return (
            <Router history={history}>
                    <div className="app-container row">
                        <ul className="admin-panel col-md-2">
                            <li><Link to="/admin/">Home</Link></li>
                            <li><Link to="/admin/pages">Pages</Link></li>
                            <li><Link to="/admin/posts">Posts</Link></li>
                        </ul>
                        <div className="admin-content col-md-10">
                            <Route exact path="/admin/" component={Home} />
                            <Route path="/admin/pages" component={Page} />
                            <Route exact path="/admin/posts/" component={Post} />
                            <Route path="/admin/posts/view/:item" component={PostDetail} />
                    </div>
                </div>
            </Router>
        );
    }
}

export default Navigation;