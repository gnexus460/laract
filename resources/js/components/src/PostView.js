import React from 'react'

class PostView extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    // componentDidMount() {
    //     fetch("/admin/posts/")
    //         .then(res => res.json())
    //         .then(
    //             (result) => {
    //                 this.setState({
    //                     isLoaded: true,
    //                     items: result
    //                 });
    //                 console.log(result);
    //             },
    //             (error) => {
    //                 this.setState({
    //                     isLoaded: true,
    //                     error
    //                 });
    //                 console.log(error);
    //             }
    //         )
    // }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-8">
                            <h1></h1>
                            <hr/>
                            <p className="lead"></p>
                        </div>
                        <div className="col-md-4">
                            <div className="card card-body bg-light">
                                <dl className="row">
                                    <dt className="col-sm-5">URL Slug:</dt>
                                    <dd className="col-sm-7"><a href=""></a></dd>
                                </dl>
                                <dl className="row">
                                    <dt className="col-sm-5">Created at:</dt>
                                    <dd className="col-sm-7"></dd>
                                </dl>
                                <dl className="row">
                                    <dt className="col-sm-5">Last uploaded:</dt>
                                    <dd className="col-sm-7"></dd>
                                </dl>
                                <hr/>
                                <div className="row">
                                    <div className="col-sm-6">
                                    </div>
                                    <div className="col-sm-6">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PostView;