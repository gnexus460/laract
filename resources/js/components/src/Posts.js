import React from 'react'
import { Router, Route, Link } from 'react-router-dom'
import PostView from './PostView'
class Posts extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("/admin/posts/")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                    console.log(result);
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                    console.log(error);
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className="col-md-12">
                    <table className="table">
                        <thead>
                        <th>#id</th>
                        <th>Title</th>
                        <th>Body</th>
                        <th>Created at</th>
                        <th></th>
                        </thead>
                        <tbody>
                        {items.map(item => (
                            <tr key={item.id}>
                                <th>{item.id}</th>
                                <td>{item.title}</td>
                                <td>{item.image}</td>
                                <td>{item.body}</td>
                                <td>
                                    <Link className="btn btn-sm btn-success view-btn" to={{
                                        pathname: "/admin/posts/view/" + item.id,
                                        state: { post_id: item.id }
                                    }}>View</Link>
                                    <a href="#"
                                       className="btn btn-sm btn-primary">Edit</a>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
            );
        }
    }
}

export default Posts;